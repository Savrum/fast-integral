const assert = require('assert');
const fi = require('..');

const wrap = (description, input, expected, condition="equal") => it(description, () => assert[condition](input, expected));

describe('Integral solve eligibility tests', () => {
  wrap("Integral even function test.", fi(1,2,-1,1), 2/3)
  wrap("Integral odd function test.", fi(1,3,-1,1), 0)
});

describe('Mathematical consistency test', () => {
  wrap("Infinite power test", fi(1,Infinity,0,1), NaN)
  wrap("Imaginary power test", fi(1,Math.sqrt(-1),0,1), NaN)
  wrap("Minefield test", !isNaN(fi(Math.random(),Math.random(),Math.random(),Math.random())), true)
  
  describe('Negative power test', () => {
    wrap("^-1 power E to E^2", fi(1,-1,Math.E,(Math.E ** 2)), 1)
    wrap("^-1 power zero to 1", fi(1,-1,0,1), -Infinity)
    wrap("Even power zero to infinity", fi(1,-2,0,Infinity), Infinity)
    wrap("Odd power zero to infinity", fi(1,-3,0,Infinity), Infinity)
    wrap("Even power -infinity to zero", fi(1,-2,-Infinity,0), Infinity)
    wrap("Odd power -infinity to zero", fi(1,-3,-Infinity,0), Infinity)
  });
  
  describe('Upper bound test', () => {
    wrap("Zero to Infinity", fi(1,1,0,Infinity), Infinity)
  });
  
  describe('Lower bound test', () => {
    wrap("-Infinity to Zero", fi(1,1,-Infinity,0), Infinity)
  });
});

describe('Data type consistency test', () => {
  wrap("Random string numeral test", fi("1",2,"-1",1), 2/3)
  wrap("Random string gibberish test", fi("A",2,"B",1), NaN)
  wrap("Boolean value test", !isNaN(fi(true,false,false,true)), true)
});

describe('Method call tests', () => {
  wrap("Empty call", isNaN(fi()), true)
});