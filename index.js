module.exports = (coefficient = 1, power = 1, start, end) => 
  (coefficient < 0 ? 0 : coefficient) // Check whether coefficient is negative
  * 
  (
    (
      (end ** power) * end
    )
    - 
    (
      Math.abs(start ** power) * start
    )
  ) / (power + 1);